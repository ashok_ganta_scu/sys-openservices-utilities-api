%dw 2.0
output application/java
---
{
	"SYS-OPENSERVICES-ACCOUNTVIEWS-API:BAD_REQUEST": {
		"errorCode": 400,
		"defaultError": vars.errorPayload,
	},
}