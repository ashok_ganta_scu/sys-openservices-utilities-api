# SCU OnPrem Template API
This template project is an attempt to minimize the amount of renaming and updating required to start a new project 

## To do list after creating new project

### Rename

#### pom.xml
* project.artifactId
* project.name
* project.properties.root ~this will be the root of your API path~

#### src/main/mule/main.xml
* mule.*flow[0].@name

#### src/main/mule/config.xml
* mule.api-gateway.@flowRef
  * to match `mule.*flow[0].@name` in `src/main/mule/main.xml`

## Expected environment variables
* mule.env
  * local
  * dev
  * UAT
  * prod
* mule.key *(deprecated)*
* mule.aes256key
* inbound.keystore.password
* inbound.keystore.keyPassword